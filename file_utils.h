#ifndef LIGO_FILE_UTILS_H
#define LIGO_FILE_UTILS_H

#include "common_utils.h"

#include <linux/fs.h>

static ssize_t write_data_to_file( const char * abs_path, int flags, int mode, const void * data, unsigned sz_bytes ) 
{
    struct file *i_fp;
    loff_t pos = 0;
    ssize_t bytes_out;
    i_fp = filp_open(abs_path, flags, mode);
    if (IS_ERR(i_fp)) {
        common_print( "intput file open error/n");
        return 0;
    }
    bytes_out = kernel_write(i_fp, data, sz_bytes, &pos);
    filp_close(i_fp, NULL);

    return bytes_out;
}



#endif //LIGO_FILE_UTILS_H
