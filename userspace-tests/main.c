
#include "hist.h"
#include "mstore.h"



int main() {
    hist_context_t * hist_ctx;
    mstore_context_t * mstore_ctx;

    int64_t ranges [] = {-30000, -2000, 2000, 15000, 30000};

    hist_ctx = hist_initialize(sizeof(ranges)/sizeof(ranges[0]), ranges);
    mstore_ctx = mstore_initialize(sizeof(uint64_t), 100);

    if ( hist_ctx == NULL || mstore_ctx == NULL ) {
        common_print("Failed to init hist (%lu) or mstore (%lu)\n", hist_ctx, mstore_ctx);
        return -1;
    }

    for (int i=0; i < 1000; ++i) {
        hist_add_element(hist_ctx, i);
        mstore_add_element(mstore_ctx, &i);
    }

    hist_add_element(hist_ctx, 100000); //Test above max
    hist_add_element(hist_ctx, -2001); //Test on bin 
    hist_add_element(hist_ctx, -100000); //Test below min


    common_print("Histogram Of All Latencies (ns)\n");
    hist_print_stats(hist_ctx);


    hist_free(hist_ctx);
    mstore_free(mstore_ctx);

    return 0;

}