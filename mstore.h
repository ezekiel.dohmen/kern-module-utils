/** 
 * @mainpage Modulus Store/ Circ. Buffer
 *
 * @section intro_sec Introduction
 * This is a kernel/user space compatible, simple mstore header only library.
 * Pass in the block size, and number of blocks to mstore_initialize(block_sz, num_blks). 
 * When you call mstore_add_element(...), block_sz will be copied from the new_ele ptr
 * into the next space available in the circular buffer. When the buffer is full it
 * will wrap around and start overwriting the oldest data. It is useful for saving the 
 * last num_blks of something. 
 *
 * @section install_sec Installation
 * Clone this repository or add it as a submodule, then add the top level directory
 * to your build includes. 
 *
 * @subsection install_dependencies Installing Dependencies
 * The only other (non-default) dependency this header has is common_utils.h
 * which should be shipped in the same repository.  
 */

#ifndef LIGO_MSTORE_H
#define LIGO_MSTORE_H


#include "common_utils.h"

#ifdef __KERNEL__
#include <linux/string.h>
#else
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#endif


typedef struct mstore_context_t
{
    int num_ele;
    int size_of_ele;
    int cur_ele;
    int num_added;
    char * block;
} mstore_context_t;

mstore_context_t * mstore_initialize(int size_of_element_bytes, int total_elements)
{
    mstore_context_t * context = (mstore_context_t *) common_malloc(sizeof(mstore_context_t));
    if(context == NULL)
        return NULL;

    context->block = (char*) common_malloc(size_of_element_bytes * total_elements);
    if(context->block == NULL)
    {
        common_free(context);
        return NULL;
    }
    memset(context->block, 0, size_of_element_bytes * total_elements);

    context->num_ele = total_elements;
    context->size_of_ele = size_of_element_bytes;
    context->cur_ele = 0;
    context->num_added = 0;

    return context;
}

void mstore_add_element(mstore_context_t * context, void * new_ele)
{
    memcpy(&context->block[context->size_of_ele * context->cur_ele], new_ele, context->size_of_ele);
    context->cur_ele = (context->cur_ele + 1) % context->num_ele;
    if(context->num_added < context->num_ele)
        ++context->num_added;
    return;
}


void * mstore_get_element_ptr(mstore_context_t * context, int index)
{
    if(index > context->num_ele)
        return 0;

    return &context->block[index * context->size_of_ele];
}

int32_t mstore_get_element_as_int32_t (mstore_context_t * ctx, int index)
{
    return *((int32_t*)mstore_get_element_ptr(ctx, index));
}

uint32_t mstore_get_element_as_uint32_t (mstore_context_t * ctx, int index)
{
    return *((uint32_t*)mstore_get_element_ptr(ctx, index));
}

int64_t mstore_get_element_as_int64_t (mstore_context_t * ctx, int index)
{
    return *((int64_t*)mstore_get_element_ptr(ctx, index));
}

uint64_t mstore_get_element_as_uint64_t (mstore_context_t * ctx, int index)
{
    return *((uint64_t*)mstore_get_element_ptr(ctx, index));
}


int mstore_get_num_elements(mstore_context_t * context)
{
    return context->num_added;
}


void mstore_free(mstore_context_t * context)
{
    common_free(context->block);
    common_free(context);
    return;
}

#endif
