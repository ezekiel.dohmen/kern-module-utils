/** 
 * @mainpage Histogram 
 *
 * @section intro_sec Introduction
 * This is a kernel/user space compatible, simple histogram header only library.
 * You need to define your bins ahead of time, call hist_initialize(size, bins),
 * then call hist_add_element(..) to add elements. The hist_add_element logic is 
 * fast (given a reasonable number of bins), and has been very useful benchmarking 
 * real-time isolated modules. 
 *
 * @section install_sec Installation
 * Clone this repository or add it as a submodule, then add the top level directory
 * to your build includes. 
 *
 * @subsection install_dependencies Installing Dependencies
 * The only other (non-default) dependency this header has is common_utils.h
 * which should be shipped in the same repository.  
 */

#ifndef LIGO_HIST_H
#define LIGO_HIST_H


#include "common_utils.h"


#ifdef __KERNEL__
#include <linux/string.h>
#else
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#endif



typedef struct hist_context_t
{
    int num_ranges;
    int64_t * ranges;
    int64_t * range_counts;
    int64_t min;
    int64_t max;
    int64_t running_sum;
    int64_t total_added;
} hist_context_t;

static void hist_free(hist_context_t *);


static inline hist_context_t * hist_initialize(int num_ranges, int64_t * range_array)
{
    uint64_t i = 0;
    hist_context_t * context;

    if(num_ranges < 2)
    {
        common_print("hist_initialize() - You must pass in at least two numbers as range bounds.\n");
        return NULL;
    }
    ++num_ranges; //We add one to store elements higher than the largest bound

    context = (hist_context_t *) common_malloc(sizeof(hist_context_t));
    if(context == NULL)
        return NULL;

    context->ranges = (int64_t*) common_malloc( sizeof(int64_t) * num_ranges);
    if(context->ranges == NULL)
    {
        common_free(context);
        return NULL;
    }

    context->range_counts = (int64_t*) common_malloc( sizeof(int64_t) * num_ranges);
    if(context->range_counts == NULL)
    {
        common_free(context->ranges);
        common_free(context);
        return NULL;
    }
    memset(context->range_counts, 0, sizeof(int64_t) * num_ranges);

    context->num_ranges = num_ranges;

    for(i = 1; i < num_ranges-2; ++i)
    {
        if(range_array[i-1] >= range_array[i])
        {
            common_print("hist_initialize() - The range array parameter must be in ascending order.\n");
            hist_free(context);
            return NULL;
        }
        context->ranges[i-1] = range_array[i-1];
    }
    context->ranges[i-1] = range_array[i-1];
    context->ranges[i] = range_array[i];
    context->ranges[i+1] = range_array[i];

    context->running_sum = 0;
    context->total_added = 0;
    context->max = 0x8000000000000000; //-9223372036854775808
    context->min = 0x7FFFFFFFFFFFFFFF; //u64 MAX

    return context;
}

static inline hist_context_t * hist_initialize_num_bins (int num_bins, int64_t bin_width, int64_t start_bin)
{

    int64_t * range_array = (int64_t*) common_malloc( sizeof(int64_t) * num_bins);
    for (int i =0; i < num_bins; ++i) {
        range_array[i] = start_bin;
        start_bin += bin_width;
    }

    hist_context_t * res = hist_initialize(num_bins, range_array);
    common_free(range_array);

    return res;
    
}


static inline void hist_add_element(hist_context_t * context, int64_t ele)
{
    uint64_t i; 

    //Collect sum for mean, and min/max checks
    context->running_sum += ele;
    context->total_added += 1;
    if ( ele > context->max ) context->max = ele;
    if ( ele < context->min ) context->min = ele;

    for(i = 0; i < context->num_ranges-1; ++i)
    {
        if(ele < context->ranges[i])
        {
            context->range_counts[i] += 1;
            return;
        }
    }

    //If we got here we are higher than the largest range
    context->range_counts[i] += 1;

    return;
}

static inline void hist_clear(hist_context_t * context)
{

    for(int i = 0; i < context->num_ranges-1; ++i)
    {
        context->range_counts[i] =0;
    }

    context->running_sum = 0;
    context->total_added = 0;
    context->max = 0x8000000000000000; //-9223372036854775808
    context->min = 0x7FFFFFFFFFFFFFFF; //u64 MAX
}

static inline int64_t hist_get_range_count(hist_context_t * context, int64_t index)
{
    if(index >= context->num_ranges)
    {
        common_print("get_range() - index parameter was out of range.\n");
        return -1;
    }

    return context->range_counts[index];
}

static inline int hist_get_num_ranges(hist_context_t * context)
{
    return context->num_ranges;
}

static inline int64_t hist_get_range_from_index(hist_context_t * context, int64_t index)
{
    if(index >= context->num_ranges)
    {
        common_print("get_range_from_index() - index parameter was out of range.\n");
        return -1;
    }
    return context->ranges[index];

}

static inline int hist_get_stats(hist_context_t * context, uint64_t * min, uint64_t * max, uint64_t * mean)
{
    if (context->total_added == 0) return -1; //No data

    *min = context->min;
    *max = context->max;
    *mean = context->running_sum/context->total_added;

    return 0;
}

static inline void hist_print_stats(hist_context_t * context)
{
    int64_t i;
    if (context->total_added > 0)
        common_print("min: %lld, max: %lld, mean: %lld\n", context->min, context->max, context->running_sum/context->total_added);
    else
        common_print("Invalid statistic data.\n");

    common_print("<%lld : %lld\n",context->ranges[0], context->range_counts[0]);
    for(i = 1; i < context->num_ranges-1; ++i)
    {
        common_print("[%lld, %lld) : %lld\n",context->ranges[i-1], context->ranges[i], context->range_counts[i] );
    }
    common_print(">%lld : %lld\n", context->ranges[context->num_ranges-1], context->range_counts[context->num_ranges-1]);
}

static void hist_free(hist_context_t * context)
{
    common_free(context->range_counts);
    common_free(context->ranges);
    common_free(context);
    return;
}

#endif

