#include <linux/module.h>
#include <linux/init.h>

#include "hist.h"
#include "mstore.h"

#include "file_utils.h"

hist_context_t * hist_ctx;
mstore_context_t * mstore_ctx;

int init( void )
{


    int64_t ranges [] = {-30000, -2000, 2000, 15000, 30000};

    hist_ctx = hist_initialize(sizeof(ranges)/sizeof(ranges[0]), ranges);
    mstore_ctx = mstore_initialize(sizeof(uint64_t), 100);

    if ( hist_ctx == NULL || mstore_ctx == NULL ) {
        common_print("Failed to init hist (%llu) or mstore (%llu)\n", (uint64_t)hist_ctx, (uint64_t)mstore_ctx);
        return -1;
    }

    for (int i=0; i < 1000; ++i) {
        hist_add_element(hist_ctx, i);
        mstore_add_element(mstore_ctx, &i);
    }

    hist_add_element(hist_ctx, 100000); //Test above max
    hist_add_element(hist_ctx, -2001); //Test on bin 
    hist_add_element(hist_ctx, -100000); //Test below min

    common_print("Histogram Of All Latencies (ns)\n");
    hist_print_stats(hist_ctx);

    write_data_to_file("/tmp/test", O_RDWR | O_CREAT, 0666, "test\n", 5);


    hist_context_t * hist_2_ctx = hist_initialize_num_bins(35, 500, 0);
    if( hist_2_ctx == NULL ) {
        common_print("Error: could not allocate hist_2_ctx\n");
        return -1;
    }
    for(int64_t i=0; i < 20000; ++i) {
        hist_add_element(hist_2_ctx, i);
    }
    common_print("Histogram Of All Latencies (ns)\n");
    hist_print_stats(hist_2_ctx);


    return 0;
}

/// Kernel module cleanup function
void cleanup( void )
{

    hist_free(hist_ctx);
    mstore_free(mstore_ctx);

}

module_init( init );
module_exit( cleanup );
