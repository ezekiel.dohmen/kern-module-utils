# Kernel Module Utils
This repository contains utilities for doing fast measurements. Mainly used for real-time benchmarks.

### Tools
- Histogram (`hist.h`)
- Modulus Store (`mstore.h`)
- Common Utils
